FROM ubuntu

# Allow buildtime config of HIVE_VERSION
ARG HIVE_VERSION
ARG HADOOP_VERSION
# Set HIVE_VERSION from arg if provided at build, env if provided at run, or default
# https://docs.docker.com/engine/reference/builder/#using-arg-variables
# https://docs.docker.com/engine/reference/builder/#environment-replacement
ENV HIVE_VERSION=${HIVE_VERSION:-3.1.2}
ENV HADOOP_VERSION=${HADOOP_VERSION:-3.3.0}
ENV HIVE_HOME /opt/hive
ENV PATH $HIVE_HOME/bin:$PATH
ENV HADOOP_HOME /opt/hadoop
ENV PATH $HADOOP_HOME/bin/:$PATH

WORKDIR /opt

#Install Hive and PostgreSQL JDBC
RUN apt-get update && apt-get install -y wget \
  procps \
  openjdk-8-jre-headless \
  netcat \
  net-tools \
  telnet \
  nano \
  less

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

RUN wget -q  https://apache-mirror.rbc.ru/pub/apache/hadoop/common/hadoop-${HADOOP_VERSION}/hadoop-${HADOOP_VERSION}.tar.gz
RUN wget -q  https://apache-mirror.rbc.ru/pub/apache/hive/hive-${HIVE_VERSION}/apache-hive-${HIVE_VERSION}-bin.tar.gz

RUN tar zxf apache-hive-${HIVE_VERSION}-bin.tar.gz
RUN ln -s /opt/apache-hive-${HIVE_VERSION}-bin ${HIVE_HOME}
RUN rm apache-hive-${HIVE_VERSION}-bin.tar.gz

RUN tar zxf hadoop-${HADOOP_VERSION}.tar.gz
RUN ln -s /opt/hadoop-${HADOOP_VERSION} ${HADOOP_HOME}
RUN rm hadoop-${HADOOP_VERSION}.tar.gz

RUN rm ${HIVE_HOME}/lib/guava*
RUN cp ${HADOOP_HOME}/share/hadoop/common/lib/guava* ${HIVE_HOME}/lib/

RUN wget -q --directory-prefix=${HIVE_HOME}/lib/ https://repository.cloudera.com/artifactory/cloudera-repos/org/apache/hive/kafka-handler/3.1.3000.7.2.8.0-228/kafka-handler-3.1.3000.7.2.8.0-228.jar

#Custom configuration goes here
ADD conf/hive-site.xml $HIVE_HOME/conf
ADD conf/beeline-log4j2.properties $HIVE_HOME/conf
ADD conf/hive-env.sh $HIVE_HOME/conf
ADD conf/hive-exec-log4j2.properties $HIVE_HOME/conf
ADD conf/hive-log4j2.properties $HIVE_HOME/conf
ADD conf/ivysettings.xml $HIVE_HOME/conf
ADD conf/llap-daemon-log4j2.properties $HIVE_HOME/conf

COPY startup.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/startup.sh

COPY entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

EXPOSE 10000
EXPOSE 10002

ENTRYPOINT ["entrypoint.sh"]
CMD startup.sh
